package chess;

import java.util.Scanner;

public class Chess {


    public static final String BLACK_FIELD = " \u25A0 ";
    public static final String WHITE_FIELD = " \u25A1 ";

    public static void main(String[] args) {
        //alt+command+v
        Scanner scanner = new Scanner(System.in);//mamy obiekt, bez odwołania do niego
        System.out.println("Podaj długość szachownicy");
        int chessLength = scanner.nextInt();
        System.out.println("Rysujesz szachownicę o długości " + chessLength);
        for (int y = 0; y < chessLength; y++) {


            for (int x = 0; x < chessLength; x++) {
                //rysujemy wiersze x
                if ((y + x) % 2 == 0) {
                    System.out.print(BLACK_FIELD); //black
                } else {

                    System.out.print(WHITE_FIELD); //white
                }
            }
            //przechodzimy do nowej linii y
            System.out.println();
        }

    }
}
