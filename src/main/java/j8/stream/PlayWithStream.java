package j8.stream;

import j8.model.ContractType;
import j8.model.Employee;
import org.w3c.dom.ls.LSOutput;

import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.*;

public class PlayWithStream {

    public static void main(String[] args) {
        List<Employee> employees = FileUtils.load(Path.of("src/main/resources/j8/employees.csv"));

//        System.out.println(employees);

//        ex1(employees);
//        ex2(employees);
//        ex3(employees);
//        ex4(employees);
//        ex5(employees);
//        ex6(employees);
//        ex7(employees);
//        ex8(employees);
//        ex9(employees);
//        ex10(employees);
//        ex11(employees);
//        ex12(employees);
//        ex13(employees);
//        ex14(employees);
//        ex14a(employees);
        ex14b(employees);
//        ex14c(employees);
//        ex15(employees);


//        //zad 1
//        List<Employee> empOver50 = employees.stream()
//                .filter(e -> e.getAge() > 50)  //starsza niz 50 lat
////                .forEach(System.out::println); //wypisuje na ekran
//                .collect(Collectors.toList());//albo statycznie collect(toList())
//
////        System.out.println(empOver50);
//
//        //zad 2
//        Map<String, String> collect = employees.stream()
//                .filter(e -> e.getAge() > 50)  //starsza niz 50 lat
////                .forEach(System.out::println); //wypisuje na ekran
//                .collect(Collectors.toMap(Employee::getFirstName, Employee::getSurName));
//
//        //zad 3
//        Map<String, List<Employee>> map = employees.stream()
//                .collect(Collectors.groupingBy(e -> e.getFirstName()));
//
////        System.out.println(map);
    }


    private static void ex15(List<Employee> employees) {
        Optional<Employee> collect = employees.stream()
                .collect(minBy(Comparator.comparing(e -> e.getSalary())));


        System.out.println(collect);

//        employees.stream()
//                .mapToDouble(e -> e.getSalary())
//                .summaryStatistics();
//
//        //summaryStatistic
//        System.out.println(IntStream.of(1, 2, 3, 4, 5, 6)
//                .summaryStatistics());
    }

    private static void ex14c(List<Employee> employees) {
        Map<String, Integer> collect = employees.stream()
                .collect(groupingBy(e -> e.getFirstName(), collectingAndThen(counting(), e -> e.intValue())));
        //collectingAndThen najpierw collector, a potem funkcja


        System.out.println(collect);
    }

    private static void ex14b(List<Employee> employees) {
        Map<String, Long> collect = employees.stream()
                .collect(groupingBy(e -> e.getFirstName(), filtering(e -> e.getFirstName().endsWith("a"), counting())));

        System.out.println(collect);
    }

    private static void ex14a(List<Employee> employees) {
        Map<String, Long> collect = employees.stream()
                .collect(groupingBy(e -> e.getFirstName(), counting()));

        System.out.println(collect);
    }

    private static void ex14(List<Employee> employees) {
        Map<String, List<Employee>> collect = employees.stream()
                .collect(groupingBy(e -> e.getFirstName()));
        System.out.println(collect);
    }

    private static void ex13(List<Employee> employees) {
//        Map<String, List<Employee>> collect = employees.stream()
////                .collect(Collectors.toMap()) -  wtedy klucz nie beda unikalne
        //mapping robi to samo co funkcja map
//                .collect(groupingBy(e -> e.getType().name()));//gdy tak skonczymy , to przypisze do listy, mapa wyglada tak jak w zeszycie
//        System.out.println(collect);

//       employees.stream()
//                .collect(groupingBy(e -> e.getType().name(), mapping(e -> e.getSalary(), Collectors.toList())));
        Map<String, Double> collect = employees.stream()
                .collect(groupingBy(e -> e.getType().name(), averagingDouble(e -> e.getSalary())));


    }

    private static void ex12(List<Employee> employees) {
        Map<Boolean, Long> collect = employees.stream()
                .collect(Collectors.partitioningBy(e -> e.getSalary() > 5000, Collectors.counting()));
        //.collect(Collectors.counting());

        System.out.println(collect);
    }

    private static void ex11(List<Employee> employees) {
        String lastNames = employees.stream()
                .map(e -> e.getLastName()) //stream po stringach, czyli po nazwisku
                .collect(Collectors.joining(", ")); //znak łączący
        System.out.println(lastNames);
    }


    private static void ex10(List<Employee> employees) {
        employees.stream()
                .sorted(Comparator.comparing(Employee::getLastName).thenComparing(Employee::getFirstName))
//                .sorted((person1, person2) -> {
//                            int lastNameComparator = person1.getLastName().compareTo(person2.getLastName());
//
//                            if (lastNameComparator != 0) {
//                                return lastNameComparator;
//                            }
//                            return person1.getFirstName().compareTo(person2.getFirstName());
//                        }
////                        Comparator.comparing(Employee::getLastName).thenComparing(Employee::getFirstName))

                .forEach(System.out::println);
    }

    private static void ex9(List<Employee> employees) {
        employees.stream()
                .filter(e -> e.getFirstName().charAt(2) == 'a')
                .filter(e -> e.getLastName().charAt(2) == 'b')
                .forEach(System.out::println);
    }

    private static void ex8(List<Employee> employees) {
        //key - surname value - salary * 1,12
//        Map<String, List<Employee>> exercise8 = employees.stream()
//                .collect(Collectors.groupingBy(e -> e.getSurName(),Collectors.mapping(e -> e.getSalary())));
//        System.out.println(exercise8);
//        Map<String, List<Double>> collect = employees.stream()
//                .collect(Collectors.toMap(e -> e.getSurName(), e -> e.getSalary()));
        Map<String, List<Double>> collect = employees.stream()
                .collect(
                        Collectors.groupingBy(e -> e.getLastName(), Collectors.mapping(e -> e.getSalary() * 1.12, toList())));
        //ten drugi to downstream collector
        System.out.println(collect);
//        Math.round(1.12);
    }

    private static void ex7(List<Employee> employees) {
        boolean isKowalska =
                employees.stream()
//                .map(e -> e.getSurName())
//                        .anyMatch(e -> e.equals("Kowalska"))
                        //znajdz jakikolwiek, nie sprawdza calego streamu, otwiera bramke dla jednego i tyle
                        .anyMatch(e -> e.getLastName().equals("Kowalska"));
        System.out.println(isKowalska);

    }


    private static void ex6(List<Employee> employees) {
        Set<String> collect = employees.stream()
                .map(e -> e.getProfession().toUpperCase())
                .collect(toSet());
//                .collect(toCollection(LinkedHashSet::new));
//                .forEach(System.out::println);
        System.out.println(collect);
    }

    private static void ex5(List<Employee> employees) {
        long count = employees.stream()
                .map(Employee::getLastName)
                .map(e -> e.substring((e.length() - 3)))
                .map(e ->
                        (e.endsWith("ski") || e.endsWith("ska"))
                                ? e.toUpperCase() : e
                )
                // w mapie moze byc warunek logiczny
                //              (boolean)     ?    true         : false
//                .forEach(System.out::println);
                .count();
        System.out.println(count);
    }

    private static void ex4(List<Employee> employees) {
        employees.stream()
                .map(Employee::getFirstName) //wjechal obiekt employee a wyjezdza tylko string i nie jestesmy w stanie wrocic do poprzednieg typu tutaj employee
                .forEach(System.out::println);
    }

    private static void ex3(List<Employee> employees) {
        employees.stream()
                .filter(e -> e.getLastName().endsWith("ska"))
                .filter(e -> e.getType().equals(ContractType.F))
                .forEach(System.out::println);
    }

    private static void ex2(List<Employee> employees) {
        employees.stream()
                .filter(e -> e.getAge() % 2 == 0)
                .forEach(System.out::println);
    }


    private static void ex1(List<Employee> employees) {
        employees.stream()
                .filter(e -> e.getSalary() > 2500
                        && e.getSalary() < 3199)
                .forEach(System.out::println);
    }
}
