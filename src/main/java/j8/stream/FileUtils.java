package j8.stream;

import j8.model.ContractType;
import j8.model.Employee;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileUtils {


    public static List<Employee> load(Path path) {
        try {
            Stream<String> linesOfCsvFile = Files.lines(path);

             return linesOfCsvFile.skip(1) //Stream<String>
                    .map(e -> e.split(",")) //Stream<String[]>
                    // .map(e -> mapToObject(e)) wersja bez method reference
                    .map(FileUtils::mapToObject) //Stream<Employee>
//                    .forEach(e -> System.out.println(Arrays.toString(e)));
//                    .forEach(e -> System.out.println(e));
//                    .forEach(System.out::println);
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
//        return null; //to nie wygląda dobrze, code stinks
        return Collections.emptyList();
    }

    public static Employee mapToObject(String[] empArray) {
        String firstName = empArray[0];
        String surName = empArray[1];
        int age = Integer.parseInt(empArray[2]);
        String profession = empArray[3];
        double salary = Double.parseDouble(empArray[4]);
        ContractType contractType = ContractType.valueOf(empArray[5]);

        return new Employee(firstName, surName, age, profession, salary, contractType);
    }
}
