package j8.functionalInterface;

import j8.model.ContractType;
import j8.model.Employee;
import j8.stream.FileUtils;

import java.nio.file.Path;
import java.text.DecimalFormat;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

public class PlayWithStreamAtHome {

    public static void main(String[] args) {

        List<Employee> employees = FileUtils.load(Path.of("src/main/resources/j8/employees.csv"));

//        System.out.println(employees);

//        ex1(employees);
//        ex2(employees);
//        ex3(employees);
//        ex4(employees);
//        ex5(employees);
//        ex6(employees);
//        ex7(employees);
//        ex8(employees);
//        ex9(employees);
//        ex10(employees);
//        ex11(employees);
//        ex12(employees);
//        ex13(employees);
//        ex14(employees);
//        ex14a(employees);
//        ex14b(employees);
//        ex14c(employees);
//        ex15(employees);
//        ex16(employees);
//        ex17(employees);
//        ex18(employees);
//        ex19(employees);
        ex20(employees);


    }

    private static void ex20(List<Employee> employees) {
        Map<String, List<Double>> collect = employees.stream()
                .collect(groupingBy(e -> e.getProfession(), filtering(e -> e.getSalary() < 2000, mapping(e -> e.getSalary(), toList()))));
        System.out.println(collect);
    }

    private static void ex19(List<Employee> employees) {
        Map<String, List<Employee>> collect = employees.stream()
                .collect(groupingBy(e -> e.getLastName(), filtering(e -> e.getFirstName().startsWith("K") == true, toList())));

        System.out.println(collect);
    }

    private static void ex18(List<Employee> employees) {
        String collect = employees.stream()
                .filter(e -> e.getFirstName().charAt(e.getFirstName().length()-1) != 'a')
                .collect(collectingAndThen(averagingDouble(e -> e.getSalary()), aDouble -> new DecimalFormat("0.00zł").format(aDouble)));
        System.out.println(collect);
    }

    private static void ex17(List<Employee> employees) {
        String a = employees.stream()
                .filter(e -> e.getFirstName().endsWith("a"))
                .collect(collectingAndThen(averagingDouble(e -> e.getSalary()), aDouble -> new DecimalFormat("0.00zł").format(aDouble)));
        System.out.println(a);

    }

    private static void ex16(List<Employee> employees) {
        Optional<String> s = employees.stream()
//                .collect(maxBy(Comparator.comparing(e -> e.getSalary())))
                .collect(minBy(Comparator.comparing(e -> e.getSalary())))
                .map(e -> e.getProfession());
        System.out.println(s);

    }

    private static void ex15(List<Employee> employees) {
        Optional<Employee> collect = employees.stream()
                .collect(maxBy(Comparator.comparing(e -> e.getSalary())));
        System.out.println(collect);
    }

    private static void ex14c(List<Employee> employees) {
        Map<String, Integer> collect = employees.stream()
                .collect(groupingBy(e -> e.getFirstName(), collectingAndThen(counting(),e -> e.intValue())));
        System.out.println(collect);

    }

    private static void ex14b(List<Employee> employees) {
        Map<String, Long> a = employees.stream()
                .filter(e -> e.getFirstName().endsWith("a"))
                .collect(groupingBy(e -> e.getFirstName(), counting()));


        System.out.println(a);
    }

    private static void ex14a(List<Employee> employees) {
        Map<String, Long> collect = employees.stream()
                .collect(groupingBy(e -> e.getFirstName(), counting()));

        System.out.println(collect);
    }


    private static void ex14(List<Employee> employees) {
        Map<String, List<Employee>> collect = employees.stream()
                .collect(groupingBy(e -> e.getFirstName(), toList()));

        System.out.println(collect);
    }

    private static void ex13(List<Employee> employees) {
        Map<String, Double> collect = employees.stream()
                .collect(groupingBy(e -> e.getType().name(), averagingDouble(e -> e.getSalary())));

        System.out.println(collect);
    }

    private static void ex12(List<Employee> employees) {
        Map<Boolean, Long> collect = employees.stream()
                .collect(partitioningBy(e -> e.getSalary() > 5000, counting()));

        System.out.println(collect);

    }

    private static void ex11(List<Employee> employees) {
        String collect = employees.stream()
                .map(e -> e.getLastName())
                .collect(joining(", "));

        System.out.println(collect);


    }

    private static void ex10(List<Employee> employees) {
        employees.stream()
                .sorted(Comparator.comparing(Employee::getLastName).thenComparing(Employee::getFirstName))
                .forEach(System.out::println);
    }

    private static void ex9(List<Employee> employees) {
        employees.stream()
                .filter(e -> e.getFirstName().charAt(1) == 'a')
                .filter(e -> e.getLastName().charAt(1) == 'o')
                .forEach(e -> System.out.println(e));

    }

    private static void ex8(List<Employee> employees) {
        Map<String, List<Double>> collect = employees.stream()

                .collect(groupingBy(e -> e.getLastName(), mapping(e -> e.getSalary() * 1.12, toList())));

//                .collect(toMap(e -> e.getLastName(), e -> e.getSalary())); - duplikaty!!!

        System.out.println(collect);

    }

    private static void ex7(List<Employee> employees) {
        boolean isKowalska = employees.stream()
//                .filter(e -> e.getLastName().equals("Kowalska"))
                .anyMatch(e -> e.getLastName().equals("Kowalska"));
        System.out.println(isKowalska);

    }

    private static void ex6(List<Employee> employees) {
        employees.stream()
                .map(e -> e.getProfession())
                .map(e -> e.toUpperCase())
                .forEach(System.out::println);
    }

    private static void ex5(List<Employee> employees) {
        employees.stream()
                .map(e -> e.getLastName())
                .map(e -> e.substring(e.length() - 3))
                .map(e -> (e.endsWith("ska") || (e.endsWith("ski"))) ? e.toUpperCase() : e)
                .forEach(System.out::println);
    }

    private static void ex4(List<Employee> employees) {
        employees.stream()
                .map(Employee::getFirstName)
                .forEach(System.out::println);
    }

    private static void ex3(List<Employee> employees) {
        employees.stream()
                .filter(e -> e.getLastName().endsWith("ska"))
                .filter(e -> e.getType().equals(ContractType.F))
                .forEach(System.out::println);
    }

    private static void ex2(List<Employee> employees) {
        employees.stream()
                .filter(e -> e.getAge() % 2 == 0)
                .forEach(System.out::println);
    }

    private static void ex1(List<Employee> employees) {
        employees.stream()
                .filter(e -> e.getSalary() > 2500 && e.getSalary() < 3199)
                .forEach(System.out::println);

    }
}
