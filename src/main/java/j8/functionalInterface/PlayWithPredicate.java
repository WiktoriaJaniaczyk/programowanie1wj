package j8.functionalInterface;

import java.util.function.Predicate;

public class PlayWithPredicate {
    public static void main(String[] args) {


        Predicate<String> checkA = (String t) -> t.contains("A");
//        System.out.println(checkA.test("Ania"));
//        System.out.println(checkA.test("Yeti"));

        Predicate<String> emptyCheck = (String t) -> t.isEmpty();
//        System.out.println(emptyCheck.test(""));
//        System.out.println(emptyCheck.test("..."));

        Predicate<String> checkAAndEmpty = checkA.and(emptyCheck);
        System.out.println(checkAAndEmpty.test(""));

    }
}
