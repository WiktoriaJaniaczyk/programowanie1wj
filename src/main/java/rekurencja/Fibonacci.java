package rekurencja;

public class Fibonacci {

    public static void main(String[] args) {
        System.out.println(countFibonacci(16));
    }

    static int countFibonacci (int indeks){
        if(indeks >= 3){
            return countFibonacci(indeks-1) + countFibonacci(indeks-2);
        }else{
            return 1;
        }
    }
}
