package lock;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj kombinację do zamknięcia zamka w formacie X-X-X"); //to nie jest int, dlatego next line
        String[] lockCombination = scanner.nextLine().split("-");
        //String[] lockSplitted = lockCombination.split("-");

//        int first = Integer.parseInt(lockCombination[0]);
//        int second = Integer.parseInt(lockCombination[1]);
//        int third = Integer.parseInt(lockCombination[2]);
//        Lock lock = new Lock(first, second, third);

        Lock lock = new Lock(Integer.parseInt(lockCombination[0]),
                Integer.parseInt(lockCombination[1]),
                Integer.parseInt(lockCombination[2]));


        System.out.println("Tworzę zamek lock. " + lock);
        lock.shuffle();
        System.out.println(lock);
        lock.switchA();
        System.out.println(lock);
        lock.switchB();
        System.out.println(lock);
        lock.switchC();
        System.out.println(lock);
        //lock.isOpen();

        if(lock.isOpen()) {
            System.out.println("Zamek jest otwarty.");
        }else{
            System.out.println("Zamek jest zamknięty.");
        }
    }
}
