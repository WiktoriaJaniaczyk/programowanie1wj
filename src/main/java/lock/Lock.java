package lock;

import java.util.Random;

public class Lock {

    private int correctA;
    private int correctB;
    private int correctC;
    private int currentA;
    private int currentB;
    private int currentC;


    public Lock(int correctA, int correctB, int correctC) {
        this.currentA = this.correctA = correctA;
        this.currentB = this.correctB = correctB;
        this.currentC = this.correctC = correctC;
    }

    public void switchA(){
        currentA = ( currentA + 1) % 10;
//        if(currentA!=9) {
//            currentA++;
//        }else{
//            currentA = 0;
//        }
    }

    public void switchB(){
        if(currentB == 9) {
            currentB = 0;
        }else{
            currentB++;
        }
    }

    public void switchC(){
        if(currentC!=9) {
            currentC++;
        }else{
            currentC = 0;
        }
    }

    boolean isOpen(){
        System.out.println("Sprawdzam czy zamek jest otwarty.");
        return currentA == correctA &&
                currentB == correctB &&
                currentC == correctC;
    }

    public void shuffle(){
        Random random = new Random();
        currentA = random.nextInt(10);
        currentB = random.nextInt(10);
        currentC = random.nextInt(10);
        System.out.println("Po shuffle: " + currentA + " " + currentB + " " + currentC+".");

    }

    @Override
    public String toString() {
        return "Aktualna kombinacja zamka: "
                + currentA + "-" + currentB + "-" + currentC+".";
    }
}
