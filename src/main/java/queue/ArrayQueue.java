package queue;

import java.util.Arrays;

public class ArrayQueue<T> implements Queue<T> {

    private Object[] elements; //tu trzeba zrobic bieda generyczność


    public ArrayQueue() {
        this.elements = new Object[0];

    }

    // [1], 2, 3
    //kopia tablicy, gdzie jej wielkość będzie zawsze size + 1
    //new ArrayQueue
    //[]
    //[] <- 145
    //size.0 -> size.0 + 1 -> size.1
    //[145]
    //200 offer
    //[145, ]
    //[] -> copyOf(stara tablica, rozmiar starej + 1)
    //[ ,] -> elements[elements.length - 1] -> 145
    //[145]
    //[145] <- 200
    //Arrays.copyOf([145],[145, ])
    //elements[elements.length - 1] -> [145, 200]

    //Arrays.copyOf()
    //elements[elements.length - 1] = element;

    @Override
    public void offer(T element) {
        //tworzymy kopie tablicy, aby zwiekszyc jej rozmiar
        elements = Arrays.copyOf(elements, elements.length + 1);
        //dodajemy element na 1 miejscu kolejki
        elements[elements.length - 1] = element;

    }


    @Override
    //pobiera pierwszy element i go zwraca i usuwa z kolejki
    public T poll() {
        //pobranie 1 elementu
        if (checkIfArrayIsEmpty()) return null;
        T element = (T) elements[0];
        elements = Arrays.copyOfRange(elements, 1, elements.length);
        return element;

    }



    @Override
    public T peek() {
        if (checkIfArrayIsEmpty()) return null;
        return (T) elements[0];
    }

    @Override
    public int size() {
        return elements.length;
    }


    private boolean checkIfArrayIsEmpty() {
        if (elements.length == 0) {
            System.out.println("Queue is empty");
            return true;
        }
        return false;
    }
}
