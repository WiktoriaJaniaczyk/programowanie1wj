package queue;

public class Link<T> {

    private T value;
    private Link<T> next;
    private Link<T> previous;


    public Link(T value) {
        this.value = value;
    }


    public Link<T> getNext() {
        return next;
    }

    public T getValue() {
        return value;
    }

    public Link<T> setNext(Link<T> next) {
        this.next = next;
        return this;
    }
}
