package queue;

public class LinkedQueue<T> implements Queue<T> {

    //czoło naszej kolejki
    private Link<T> head;


    @Override
    //sprawdzic czy mamy czolo kolejki
    public void offer(T element) {
        //sprawdzenie czy mamy czolo kolejki
        //jesli nie to ustawic na nim nowy link
        //jesli mamy czolo koleki to w petli przebiegamy po nastepnych ogniwach
        //czyli daj nastepnego nic nie zwraca

        //[LINK, LINK, LINK, (pusto, czyli tutaj go wrzuci) 50 ] <- offer 50
        //[LINK -> daj nastepnego, LINK -> daj nastepnego, ...]
        Link<T> newLink = new Link<>(element);
        if(head == null){
            head = newLink;
            //jesli nie mamy czola kolejki to je ustawiamy
        }else{
            //mamy czolo, przebiegamy w petli
            //dopoki badane ogniwo ma kolejny element
            //idziemy do "ogona"
            Link<T> link = this.head;
            while(link.getNext() != null){
                //iterujemy az dojedziemy do ogona
                link = link.getNext();
            }
            link.setNext(newLink);

        }
    }

    @Override
    public T poll() {
        //jeżeli kolejka jest pusta
        if(head == null){
            System.out.println("Empty queue");
            return null;
        }
        //jezeli jest to pobieramy wartosc heada
        T headValue = head.getValue();
        head = head.getNext();
        return headValue;

    }

    @Override
    public T peek() {
        return (T) head.getValue();
    }
//do DOMU!!! i double linked list :)
    @Override
    public int size() {

        return 0;
    }
}
