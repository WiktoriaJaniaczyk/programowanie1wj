package anagram;

import java.util.Arrays;
import java.util.Scanner;

public class Anagram {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj 2 słowa, żeby sprawdzić czy są anagramami");
        System.out.println("Podaj pierwsze słowo: ");
        String firstWord = scanner.nextLine();
        firstWord = firstWord.trim();
        System.out.println("Podaj drugie słowo: ");
        String secondWord = scanner.nextLine();
        secondWord = secondWord.trim();

        char[] firstWordCharacters = firstWord.toCharArray();
        Arrays.sort(firstWordCharacters);
        //System.out.println(firstWordCharacters);
        char [] secondWordCharacters = secondWord.toCharArray();
        Arrays.sort(secondWordCharacters);

        boolean areEqual = Arrays.equals(firstWordCharacters,secondWordCharacters);

        if(areEqual){
            System.out.println("Podane słowa są anagramami");
        }else{
            System.out.println("Podane słowa nie są anagramami");
        }


    }
}
