package calculator;

import java.util.function.IntBinaryOperator;

public enum Calculator {

    ADD("Dodawanie", (x,y) -> x + y),
    MULTIPLY("Mnożenie", (x,y) -> x * y),
    SUBTRACT("Odejmowanie", new IntBinaryOperator() {
        @Override
        public int applyAsInt(int left, int right) {
            return left + right;
        }
    });


//    MULTIPLY("Mnożenie") {
//        @Override
//        public int calculate(int a, int b) {
//            return a * b;
//        }
//    },
//    SUBTRACT("Odejmowanie") {
//        @Override
//        public int calculate(int a, int b) {
//            return a - b;
//        }
//    };

    private String description;
    private IntBinaryOperator operator;

    Calculator(String description, IntBinaryOperator operator) {
        this.operator = operator;
        this.description = description;

    }
        public int calculate (int a, int b){ //usuwamy abstract
        return operator.applyAsInt(a,b);
        }


}