package calculator;

public enum Example {
    RED("CZE"){
        @Override
        public void something() {
            System.out.println("Something RED");
        }
    },

    GREEN("ZIE"){
        @Override
        public void something() {
            System.out.println("Something GREEN");
        }
    },
    BLUE("NIE"){
        @Override
        public void something() {
            System.out.println("Something BLUE");

        }
    };

    private String desc;

    Example(String desc){
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public abstract void something();
}

