package queueAtHome;

public interface QueueAtHome {

    void offer(int element);
    int poll();
    int peek();
    int size();
}
