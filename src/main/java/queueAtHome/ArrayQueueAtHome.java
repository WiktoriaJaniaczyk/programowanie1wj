package queueAtHome;

import java.util.Arrays;

public class ArrayQueueAtHome implements QueueAtHome {

    private int[] elements;

    public ArrayQueueAtHome() {
        //tworzymy tablice o rozmiarze 0
        //ustawiamy rozmiar tablicy na 0
        this.elements = new int[0];
    }

    @Override
    public void offer(int element) {
        elements = Arrays.copyOf(elements, elements.length + 1);
        elements[elements.length - 1] = element;
    }

    @Override
    public int poll() {
        if(checkIfArrayIsEmpty()) return 0;
        int element = elements[0];
        elements = Arrays.copyOfRange(elements, 1, elements.length);
        return element;
    }

    @Override
    public int peek() {
        if(checkIfArrayIsEmpty()) return 0;
        int element = elements[0];
        return element;
    }

    @Override
    public int size() {
        return elements.length;
    }

    private boolean checkIfArrayIsEmpty() {
        if(elements.length == 0){
            System.out.println("Queue is empty");
            return true;
        }
            return false;
    }
}
