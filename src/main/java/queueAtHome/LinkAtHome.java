package queueAtHome;


public class LinkAtHome {

    private int value;
    private LinkAtHome next;

    public LinkAtHome(int value) {
        this.value = value;
    }


    public int getValue() {
        return value;
    }

    public LinkAtHome getNext() {
        return next;
    }

    public LinkAtHome setNext(LinkAtHome next) {
        this.next = next;
        return this;
    }
}
