package queueAtHome;

import queue.Link;

public class LinkedQueueAtHome implements QueueAtHome {

    private LinkAtHome head;

    @Override
    public void offer(int element) {
        LinkAtHome linkAtHome = new LinkAtHome(element);

        if(head == null){
            head = linkAtHome;
        }else{
            LinkAtHome temp = this.head;
            while(temp.getNext() != null){
                temp = temp.getNext();
            }
            temp.setNext(linkAtHome);
        }

    }

    @Override
    public int poll() {
        if(checkIfQueueIsEmpty()) return 0;
        head.getValue();
        this.head = head.getNext();
//        LinkAtHome temp = this.head;

        return 0;
    }

    @Override
    public int peek() {
        if(checkIfQueueIsEmpty())  return 0;
        return head.getValue();
    }



    @Override
    public int size() {
        if(checkIfQueueIsEmpty()) return 0;
        LinkAtHome temp = this.head;
        int i=1;
        while(temp.getNext() != null){
            temp = temp.getNext();
            i++;
        }
        return i;
    }

    private boolean checkIfQueueIsEmpty() {
        if(head == null){
            System.out.println("Queue is empty");
            return true;
        }
        return false;
    }
}
