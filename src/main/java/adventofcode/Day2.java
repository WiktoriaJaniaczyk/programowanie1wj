package adventofcode;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Day2 {
    public static void main(String[] args) throws IOException {


        String inputData = Files.readString(Path.of("src/main/resources/adventofcode/day2input"));
        int[] numbers = Arrays.stream(inputData.split(",")).mapToInt(Integer::parseInt).toArray();

//        for (int s:numbers) {
//            System.out.println(s);
//        }

//        Stream<String> inputValues = Files.lines(Path.of("src/main/resources/adventofcode/day2input"));
//        List<String> inputData = inputValues.collect(Collectors.toList());
////        List<String> inputData = inputValues.map(e -> e.split(",")).map(e -> Integer(e)).collect(Collectors.toList());

//       List<Integer> inputData =new ArrayList<>(Arrays.asList(1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50, 12, 13));
//       List<Integer> inputData =new ArrayList<>(Arrays.asList(1, 0, 0, 0, 99));
//       List<Integer> inputData =new ArrayList<>(Arrays.asList(2, 3, 0, 3, 99));
//       List<Integer> inputData =new ArrayList<>(Arrays.asList(2, 4, 4, 5, 99, 0));
//       List<Integer> inputData =new ArrayList<>(Arrays.asList(1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,1,13,19,2,9,19,23,1,23,6,27,1,13,27,31,1,31,10,35,1,9,35,39,1,39,9,43,2,6,43,47,1,47,5,51,2,10,51,55,1,6,55,59,2,13,59,63,2,13,63,67,1,6,67,71,1,71,5,75,2,75,6,79,1,5,79,83,1,83,6,87,2,10,87,91,1,9,91,95,1,6,95,99,1,99,6,103,2,103,9,107,2,107,10,111,1,5,111,115,1,115,6,119,2,6,119,123,1,10,123,127,1,127,5,131,1,131,2,135,1,135,5,0,99,2,0,14,0));
//      List<Integer> inputData =new ArrayList<>(Arrays.asList(2, 4, 4, 5, 99, 0,1,2,2,4,5,6,7,99));
//       ArrayList<Integer> inputData = new ArrayList<Integer>(Arrays.asList(1, 1, 1, 4, 99, 5, 6, 0, 99));
//            ArrayList<Integer> inputData = new ArrayList<Integer>();
//            inputData.add(1);
//            inputData.add(1);
//            inputData.add(1);
//            inputData.add(4);
//            inputData.add(99);
//            inputData.add(5);
//            inputData.add(6);
//            inputData.add(0);
//            inputData.add(99);
//        System.out.println(inputData);

//        682644
//        7594646
//
//        inputData.set(2, 2);
//        inputData.set(1, 12);
        numbers[2] = 76;
        numbers[1] = 33;
//        System.out.println(numbers[2]);
//        System.out.println(numbers);
//        for (String s: inputData) {
//            System.out.print(s + " ");
//        }

        for (int i = 0; i < numbers.length; i++) {
            if (i % 4 == 0) {
//                System.out.println(i);
                switch (numbers[i]) {
                    case 1:
                        //System.out.println(add(inputData.get(inputData.get(i + 1)), inputData.get(inputData.get(i + 2))));
//                        inputData.set(inputData.get(i + 3), add(inputData.get(inputData.get(i + 1)), inputData.get(inputData.get(i + 2))));
                        numbers[numbers[i + 3]] = numbers[numbers[i + 1]] + numbers[numbers[i + 2]];
//                        inputData[i+3] = String.valueOf(add(Integer.parseInt(inputData[i+1]),Integer.parseInt(inputData[i+2])));
//                        System.out.println(numbers);
//                        for (int s : numbers) {
//                            System.out.print(s+", ");
//                        }
                        break;
                    case 2:
//                        System.out.println(multiply(inputData.get(inputData.get(i + 1)), inputData.get(inputData.get(i + 2))));
//                        inputData.set(inputData.get(i + 3), multiply(inputData.get(inputData.get(i + 1)), inputData.get(inputData.get(i + 2))));
                        numbers[numbers[i + 3]] = numbers[numbers[i + 1]] * numbers[numbers[i + 2]];
//                        inputData[i+3] = String.valueOf(multiply(Integer.parseInt(inputData[i+1]),Integer.parseInt(inputData[i+2])));
//                        System.out.println(numbers);
//                        for (int s : numbers) {
//                            System.out.print(s+", ");
//                        }
                        break;
                    case 99:
                        stop();
                        i = numbers.length;
                        break;
                    default:
                        System.out.println("Something went wrong");
                        break;
                }
//
            }

        }
//        for (String s: inputData) {
//            System.out.print(s + " ");
//        }
        for (int s : numbers) {
            System.out.print(s+", ");
        }
    }

    public static int add(int a, int b) {
        return a + b;
    }

    public static int multiply(int a, int b) {
        return a * b;
    }

    public static int stop() {
        System.out.println("The end");
        return 0;
    }
}

