package hospital;

public enum Illness {
    FLU(1),
    COLD(2),
    DIARRHEA(3),
    STH_SERIOUS(4);

    private int infectiousness;

    Illness(int infectiousness) {
        this.infectiousness = infectiousness;
    }

    public int getInfectiousness() {
        return infectiousness;
    }
}
