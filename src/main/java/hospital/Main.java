package hospital;

import java.awt.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
//        System.out.println("Witaj w przychodni, wybierz:\n" +
//                "1 - rejestracja pacjenta\n" +
//                "2 - obsługa pacjenta\n" +
//                "3 - liczba pacjentów w kolejce\n" +
//                "4 - sprawdż, kto jest następny w kolejce\n" +
//                "q - wyjście");


        String option;
        HospitalQueueService hospitalQueueService = new HospitalQueueService();

        do {
            printMenu();
             option = scanner.nextLine();
            switch (option) {
                case "1":
                    System.out.println("Rejestracja nowego pacjenta");
                    Patient newPatient = handleNewPatient(scanner);
                    hospitalQueueService.addPatient(newPatient);
                    break;
                case "2":
                    Patient handledPatient = hospitalQueueService.handlePatient();
                    printPatientInfo(handledPatient);
                    break;
                case "3":
                    System.out.println(hospitalQueueService.queueSize());
                    break;
                case "4":
                    System.out.println(hospitalQueueService.nextPatient());
                    break;
                case "q":
                    break;
            }
        } while (!"q".equals(option));

        }

    private static void printMenu() {
        System.out.println( "Witaj w przychodni, wybierz:\n" +
                "1 - rejestracja pacjenta\n" +
                "2 - obsługa pacjenta\n" +
                "3 - liczba pacjentów w kolejce\n" +
                "4 - sprawdż, kto jest następny w kolejce\n" +
                "q - wyjście");
    }


    private static void printPatientInfo(Patient handledPatient) {
        System.out.println(new StringBuilder()
                .append("Pacjent ")
                .append(handledPatient.getFirstName())
                .append(" ")
                .append(handledPatient.getLastName())
                .append(" "+"został przyjęty")
                .toString()
        );

    }

    private static Patient handleNewPatient(Scanner scanner) {
        System.out.println("Imie: ");
        String firstName = scanner.nextLine();
        System.out.println("Nazwisko: ");
        String lastName = scanner.nextLine();
        System.out.println("Zlosc: ");
        int howAngry = scanner.nextInt();
        System.out.println("Choroba: ");
        scanner.nextLine();
        String illnessStringVal = scanner.nextLine();
        Illness illness = Illness.valueOf(illnessStringVal);

        Patient patient = new Patient();
        patient.setFirstName(firstName);
        patient.setLastName(lastName);
        patient.setHowAngry(howAngry);
        patient.setIllness(illness);

        return patient;
    }
}
