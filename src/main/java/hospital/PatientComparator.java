package hospital;

import java.util.Comparator;

public class PatientComparator implements Comparator<Patient> {

    private final static String KOWALSKI = "Kowalski";

    @Override
    public int compare(Patient o1, Patient o2) {
        // -1 gdy o1 > o2
        //0 gdy o1 = o2
        //1 gdy o1 < o2
        boolean isFirstKowalski = o1.getLastName().equals(KOWALSKI);
        boolean isSecondKowalski = o2.getLastName().equals(KOWALSKI);

        if (!isFirstKowalski && isSecondKowalski) {
            return 1;
        } else if (isFirstKowalski && !isSecondKowalski) {
            return -1;
        } else {

            boolean isFirstSthSerious = o1.getIllness().equals(Illness.STH_SERIOUS);
            boolean isSecondSthSerious = o2.getIllness().equals(Illness.STH_SERIOUS);

            if (!isFirstSthSerious && isSecondSthSerious) {
                return 1;
            } else if (isFirstSthSerious && !isSecondSthSerious) {
                return -1;
            } else {
                Integer firstFactor = o1.getIllness().getInfectiousness() * o1.getHowAngry();
                Integer secondFactor = o2.getIllness().getInfectiousness() * o2.getHowAngry();

                return secondFactor.compareTo(firstFactor);

            }
        }
    }
}