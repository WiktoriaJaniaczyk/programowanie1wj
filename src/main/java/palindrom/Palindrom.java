package palindrom;

import java.util.Scanner;

public class Palindrom {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj słowo: ");
        String word = scanner.nextLine();
        System.out.println(word);
        word = word.trim();
        //System.out.println(word.charAt(4));

        boolean isPalindrom = true;
        int i = 0;
        int j = word.length() - 1;

        while(i <= j) {
            //System.out.println("Checking " + word.charAt(i) + " vs " + word.charAt(j));
            if (word.charAt(i) != word.charAt(j)) {
                isPalindrom = false;
                break;
            }
            i++;
            j--;
        }
        if(isPalindrom){
            System.out.println("Podane słowo " + word + " jest palindromem");
        }else{
            System.out.println("Podane słowo " + word + " nie jest palindromem");
        }


    }
}
